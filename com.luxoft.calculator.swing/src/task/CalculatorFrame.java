package task;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class CalculatorFrame extends JFrame  {

    private DefaultTableModel model;

    CalculatorFrame() {
        super("SWT Calculator");
        this.setSize(600, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        model = new DefaultTableModel();
        model.addColumn("First number");
        model.addColumn("Type of operation");
        model.addColumn("Second number");
        model.addColumn("Result");

        JPanelCalculator panelCalculator = new JPanelCalculator(model);
        JPanelHistory panelHistory = new JPanelHistory(model);

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Calculator", panelCalculator);
        tabbedPane.addTab("History", panelHistory);
        this.add(tabbedPane);
    }
}

