package task;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class JPanelCalculator extends JPanel implements ItemListener {

    private static final String[] actions = new String[]{"+", "-", "/", "*"};

    private DefaultTableModel model;
    private Calculate calculator = new Calculate();

    private JTextField num1Field, num2Field, resultField;
    private JLabel textResult;
    private JComboBox operationWithNums;
    private JCheckBox flyCalculator;
    private JButton calculate;

    private ActionListener actionListener = this::buttonCalculatePress;
    private DocumentListener documentListener = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            toDo();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            toDo();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            toDo();
        }
    };

    public JPanelCalculator(DefaultTableModel model) {
        this.model = model;
        this.setSize(500,250);

        num1Field = new JTextField("", 10);
        num2Field = new JTextField("", 10);
        textResult = new JLabel("Result:");
        resultField = new JTextField("", 20);
        operationWithNums = new JComboBox(actions);
        flyCalculator = new JCheckBox("Calculate on the fly");
        calculate = new JButton("Calculate");

        JPanel panelResult = new JPanel();
        panelResult.setLayout(new FlowLayout());
        panelResult.add(textResult);
        panelResult.add(resultField);

        JPanel panelCalculate = new JPanel();
        panelCalculate.setLayout(new BorderLayout());
        panelCalculate.add(flyCalculator, BorderLayout.WEST);
        panelCalculate.add(calculate, BorderLayout.EAST);

        JPanel panelAddNums = new JPanel();
        panelAddNums.setLayout(new BorderLayout());
        panelAddNums.add(num1Field, BorderLayout.WEST);
        panelAddNums.add(operationWithNums, BorderLayout.CENTER);
        panelAddNums.add(num2Field, BorderLayout.EAST);

        this.setLayout(new BorderLayout());
        this.add(panelResult,BorderLayout.SOUTH);
        this.add(panelCalculate, BorderLayout.CENTER);
        this.add(panelAddNums, BorderLayout.NORTH);

        calculate.addActionListener(this::buttonCalculatePress);
        flyCalculator.addItemListener(this);

        num1Field.getDocument().addDocumentListener(documentListener);

        num2Field.getDocument().addDocumentListener(documentListener);

    }

    private void buttonCalculatePress (ActionEvent e) {
        calculate();
    }

    private void calculate(){
        try {
            double result = calculator.operation(Double.parseDouble(num1Field.getText()),
                    actions[operationWithNums.getSelectedIndex()], Double.parseDouble(num2Field.getText()));
            resultField.setText("" + result);
            model.insertRow(model.getRowCount(), new String [] {num1Field.getText(),
                    actions[operationWithNums.getSelectedIndex()], num2Field.getText(), result + ""});
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "NOT NUMBER!", "ALERT", JOptionPane.WARNING_MESSAGE);
            num1Field.setText("");
            num2Field.setText("");
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if(flyCalculator.isSelected()) {
            calculate.setEnabled(false);
            operationWithNums.addActionListener(actionListener);
            calculate();
        } else {
            calculate.setEnabled(true);
            operationWithNums.removeActionListener(actionListener);
        }
    }

    public void toDo(){
        if(flyCalculator.isSelected() &&!num1Field.getText().equals("") && !num2Field.getText().equals(""))
            calculate();
    }
}

