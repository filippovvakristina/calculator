package task;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JPanelHistory extends JPanel {

    private DefaultTableModel model;
    private JTable tableOperation;

    public JPanelHistory(DefaultTableModel model) {
        this.model = model;
        this.setSize(500,250);
        JPanel panelHistoryTable = new JPanel();
        tableOperation = new JTable(model);
        JScrollPane jScrollPane = new JScrollPane(tableOperation);
        panelHistoryTable.add(jScrollPane);

        JPanel panelHistoryButtons = new JPanel();
        panelHistoryButtons.setLayout(new BorderLayout());
        JButton save = new JButton("Save");
        JButton clear = new JButton("Clear");

        panelHistoryButtons.add(save, BorderLayout.WEST);
        panelHistoryButtons.add(clear, BorderLayout.EAST);

        save.addActionListener(this::buttonSavePress);
        clear.addActionListener(this::buttonClearPress);

        this.setLayout(new BorderLayout());
        this.add(panelHistoryTable, BorderLayout.NORTH);
        this.add(panelHistoryButtons, BorderLayout.SOUTH);
    }

    private void buttonSavePress (ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to save");

        int userSelection = fileChooser.showSaveDialog(this);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            System.out.println("Save as file: " + fileToSave.getAbsolutePath());
            try {
                FileWriter fw = new FileWriter(fileToSave);
                BufferedWriter bw = new BufferedWriter(fw);

                for (int i = 0; i < model.getColumnCount(); i++) {

                    bw.write(model.getColumnName(i));
                    bw.write("\t");

                }
                for (int i = 0; i < model.getRowCount(); i++) {
                    bw.newLine();
                    for (int j = 0; j < model.getColumnCount(); j++) {
                        bw.write((String) model.getValueAt(i,j));
                        String val = (String) model.getValueAt(i, j);
                        if (val == null) {
                            val = "";
                        }
                        bw.write("\t");
                    }
                }
                bw.close();
                fw.close();

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Error", "Error Message", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void buttonClearPress (ActionEvent e) {
        DefaultTableModel dm = (DefaultTableModel)tableOperation.getModel();
        dm.getDataVector().removeAllElements();
        dm.fireTableDataChanged();
    }
}
